// // 1. Function penghitungan jumlah kata
//         var kalimat_1 = "Halo nama saya Muhammad Iqbal Mubarok"
//         var kalimat_2 = "Saya Iqbal"
//         var kalimat_3 = "Saya Muhammad Iqbal Mubarok"

//         function jumlah_kata(str) {
//             var x =str.split(" ")
//             console.log(x.length);
//         }

//         jumlah_kata(kalimat_1) // 6
//         jumlah_kata(kalimat_2) // 2
        // jumlah_kata(kalimat_3) // 4

// 2. Function Penghasil Tanggal Hari Esok

var hari31 = [1, 3, 5, 7, 8, 10, 12]
var hari30 = [4, 6, 9, 11]

function next_date(tanggal, bulan, tahun){
    if (tanggal < 32 && bulan < 13 && tanggal > 0 && bulan > 0){ 
        tanggal = tanggal + 1
        if (hari31.includes(bulan) && tanggal > 31){
            tanggal = 1
            if (bulan == 12){
                bulan = 1
                tahun += 1
            } else {
                bulan += 1
            }
        } 
        else if (hari30.includes(bulan) && tanggal > 30){
            bulan += 1
            tanggal = 1
        }
        else if (bulan==2){
            if (tanggal < 31) {
                if(tahun%4==0 && tanggal>29){
                    bulan = 3
                    tanggal = 1
                } else if (tahun%4!=0 && tanggal>28) {
                    bulan = 3
                    tanggal = 1
                }
            } else {
                return console.error("Please insert valid date!");
            }
        }
        switch (bulan) {
            case 1:
                bulan = "Januari"
                break;
            case 2:
                bulan = "Februari"
                break;
            case 3:
                bulan = "Maret"
                break;
            case 4:
                bulan = "April"
                break;
            case 5:
                bulan = "Mei"
                break;
            case 6:
                bulan = "Juni"
                break;
            case 7:
                bulan = "Juli"
                break;
            case 8:
                bulan = "Agustus"
                break;
            case 9:
                bulan = "September"
                break;
            case 10:
                bulan = "Oktober"
                break;
            case 11:
                bulan = "November"
                break;
            case 12:
                bulan = "Desember"
                break;
                
            default:
                break;
        }
        var besok = tanggal + " " + bulan + " " + tahun
        console.log(besok);
    } else {
        console.error("Please insert valid date!");
    }
}

var tanggal = 29
var bulan = 2
var tahun = 2020
next_date(tanggal , bulan , tahun ) // output : 1 Maret 2020

var tanggal = 28
var bulan = 2
var tahun = 2021

next_date(tanggal , bulan , tahun ) // output : 1 Maret 2021


var tanggal = 31
var bulan = 12
var tahun = 2020

next_date(tanggal , bulan , tahun )// output : 1 Januari 2021
