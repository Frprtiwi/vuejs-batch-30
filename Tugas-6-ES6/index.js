// Nomor 1

        let Luas_persegipanjang = (panjang, lebar) => {
            let x = panjang * lebar
            console.log(x);
        }
        Luas_persegipanjang(5,3)

// Nomor 2

        const newFunction = (firstName, lastName) => {
            return {
            firstName,
            lastName,
            fullName(){
                console.log(firstName + " " + lastName)
            }
            }
        }

        newFunction("William", "Imoh").fullName()

// Nomor 3

        let Nama = {
            firstName: "Muhammad",
            lastName: "Iqbal Mubarok",
            address: "Jalan Ranamanyar",
            hobby: "playing football",
        }

        let {firstName, lastName, address, hobby} = Nama
        console.log(firstName +" " + lastName + " " + address + " " + hobby);

// Nomor 4

        const west = ["Will", "Chris", "Sam", "Holly"]
        const east = ["Gill", "Brian", "Noel", "Maggie"]
        let combined = [...west, ...east]
        console.log(combined);

// Nomor 5

        const planet = "earth" 
        const view = "glass" 
        let before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} `
        console.log(before);

